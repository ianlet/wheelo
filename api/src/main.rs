#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate wheelo;
extern crate uuid;
extern crate rocket;
extern crate serde_json;
#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;

use wheelo::application::rides::{RequestRideCommand, RequestRideHandler};
use wheelo::persistence::in_memory::InMemoryRideRepository;
use uuid::Uuid;
use rocket::response::Response;
use rocket::http::Status;
use rocket::http::hyper::header;
use rocket_contrib::JSON;

trait Created<'a> {
  fn created(url: String) -> Result<Response<'a>, Status>;
}

impl<'a> Created<'a> for Response<'a> {
  fn created(url: String) -> Result<Response<'a>, Status> {
    let mut response = Response::build();
    response.status(Status::Created).header(header::Location(url)).ok()
  }
}

#[derive(Serialize, Deserialize)]
struct RequestRideDto {
  passenger_id: String,
  departure_lat: f64,
  departure_long: f64,
  arrival_lat: f64,
  arrival_long: f64,
}

#[post("/", format = "application/json", data = "<dto>")]
fn request_ride<'a>(dto: JSON<RequestRideDto>) -> Result<Response<'a>, Status> {
  let ride_number = Uuid::new_v4();
  let command = RequestRideCommand {
    ride_number: ride_number.to_string(),
    passenger_id: dto.passenger_id.to_owned(),
    departure_lat: dto.departure_lat,
    departure_long: dto.departure_long,
    arrival_lat: dto.arrival_lat,
    arrival_long: dto.arrival_long,
  };

  let ride_repository = InMemoryRideRepository::new();
  let handler = RequestRideHandler::new(Box::new(ride_repository));
  handler.handle(command);

  let url = format!("/rides/{}", ride_number.to_string());
  Response::created(url)
}

fn main() {
  rocket::ignite().mount("/rides", routes![request_ride]).launch();
}
