use ::domain::rides::{Ride, RideRepository, RideNumber, PassengerId, Coordinates};

pub struct RequestRideCommand {
  pub ride_number: String,
  pub passenger_id: String,
  pub departure_lat: f64,
  pub departure_long: f64,
  pub arrival_lat: f64,
  pub arrival_long: f64,
}

impl RequestRideCommand {
  fn to_domain(self) -> (RideNumber, PassengerId, Coordinates<f64>, Coordinates<f64>) {
    let ride_number = RideNumber::new(self.ride_number);
    let passenger_id = PassengerId::new(self.passenger_id);
    let departure = Coordinates::new(self.departure_lat, self.departure_long);
    let arrival = Coordinates::new(self.arrival_lat, self.arrival_long);
    (ride_number, passenger_id, departure, arrival)
  }
}

pub struct RequestRideHandler {
  ride_repository: Box<RideRepository>,
}

impl RequestRideHandler {
  pub fn new(ride_repository: Box<RideRepository>) -> Self {
    RequestRideHandler { ride_repository: ride_repository }
  }

  pub fn handle(&self, command: RequestRideCommand) {
    let (ride_number, passenger_id, departure, arrival) = command.to_domain();
    let ride = Ride::new(ride_number, passenger_id, departure, arrival);
    self.ride_repository.persist(ride);
  }
}
