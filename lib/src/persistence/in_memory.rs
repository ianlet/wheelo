use ::domain::rides::{Ride, RideRepository};

pub struct InMemoryRideRepository;

impl InMemoryRideRepository {
  pub fn new() -> Self {
    InMemoryRideRepository {}
  }
}

impl RideRepository for InMemoryRideRepository {
  fn persist(&self, ride: Ride) {}
}
