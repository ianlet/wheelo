pub struct RideNumber {
  number: String,
}

impl RideNumber {
  pub fn new(number: String) -> Self {
    RideNumber { number: number }
  }
}

pub struct PassengerId {
  id: String,
}

impl PassengerId {
  pub fn new(id: String) -> Self {
    PassengerId { id: id }
  }
}

pub struct Coordinates<T> {
  latitude: T,
  longitude: T,
}

impl<T> Coordinates<T> {
  pub fn new(latitude: T, longitude: T) -> Self {
    Coordinates {
      latitude: latitude,
      longitude: longitude,
    }
  }
}

pub struct Ride {
  number: RideNumber,
  passenger_id: PassengerId,
  departure: Coordinates<f64>,
  arrival: Coordinates<f64>,
}

impl Ride {
  pub fn new(number: RideNumber, passenger_id: PassengerId, departure: Coordinates<f64>, arrival: Coordinates<f64>) -> Self {
    Ride {
      number: number,
      passenger_id: passenger_id,
      departure: departure,
      arrival: arrival,
    }
  }
}

pub trait RideRepository {
  fn persist(&self, ride: Ride);
}
